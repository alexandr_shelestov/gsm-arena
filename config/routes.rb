Rails.application.routes.draw do
  root to: 'welcome#index'

  get 'fetch_elements', to: 'welcome#fetch_elements'
  get 'search_elements', to: 'welcome#search_elements'
end
