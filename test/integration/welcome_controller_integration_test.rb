require 'test_helper'

class WelcomeControllerIntegrationTest < ActionDispatch::IntegrationTest

  context '#fetch_elements' do
    should 'returns hash for initial menu' do
      VCR.use_cassette 'gsm_arena/fetch_elements_0' do
        get '/fetch_elements', xhr: true, params: { website: 'gsm_arena', level: 0 }

        @data = JSON.parse(response.body)

        assert_equal 'menu', @data['fetched_content']['render_content']
        assert_equal 36, @data['fetched_content']['content'].length
        assert_equal 'Acer', @data['fetched_content']['content'][0]['caption']
        assert_equal 'http://www.gsmarena.com/zte-phones-62.php', @data['fetched_content']['content'][-1]['href']
      end
    end

    should 'returns hash for the second menu level' do
      VCR.use_cassette 'gsm_arena/fetch_elements_1' do
        get '/fetch_elements', xhr: true, params: {
          website: 'gsm_arena', level: 1,
          custom_url: 'http://www.gsmarena.com/zte-phones-62.php'
        }

        @data = JSON.parse(response.body)

        assert_equal 'menu', @data['fetched_content']['render_content']
        assert_equal 200, @data['fetched_content']['content'].length
        assert_equal 'Avid Plus', @data['fetched_content']['content'][0]['caption']
        assert_equal 'http://www.gsmarena.com/zte_zmax_pro-8096.php', @data['fetched_content']['content'][-1]['href']
      end
    end

    should 'returns hash for the third level — the Page' do
      VCR.use_cassette 'gsm_arena/fetch_elements_2' do
        get '/fetch_elements', xhr: true, params: {
          website: 'gsm_arena', level: 2,
          custom_url: 'http://www.gsmarena.com/zte_zmax_pro-8096.php'
        }

        @data = JSON.parse(response.body)

        assert_equal 'page', @data['fetched_content']['render_content']
        assert_equal 3, @data['fetched_content']['content'].length
        assert_equal 'text', @data['fetched_content']['content'][0]['type_of_content']
        assert_equal 'h1', @data['fetched_content']['content'][0]['content'][0]['tag']
        assert_equal 'ZTE Zmax Pro', @data['fetched_content']['content'][0]['content'][0]['text']
      end
    end
  end

  context '#search_elements' do
    should 'returns hash for search string Alcatel' do
      VCR.use_cassette 'gsm_arena/search_elements_alcatel' do
        get '/search_elements', xhr: true, params: { website: 'gsm_arena', search_string: 'alcatel' }

        @data = JSON.parse(response.body)

        assert_equal 'card', @data['found_content']['render_content']
        assert_equal 40, @data['found_content']['content'].length
        assert_equal 'image', @data['found_content']['content'][0][0]['type_of_content']
        assert_equal 'http://cdn2.gsmarena.com/vv/bigpic/zte-axon-7-mini.jpg',
                     @data['found_content']['content'][0][0]['content'][0]['src']
        assert_equal 'link', @data['found_content']['content'][0][1]['type_of_content']
        assert_equal 'http://www.gsmarena.com/zte_axon_7_mini-8232.php',
                     @data['found_content']['content'][0][1]['content'][0]['href']
      end
    end

    should 'returns hash for search string Iphone Plus' do
      VCR.use_cassette 'gsm_arena/search_elements_iphone_3g' do
        get '/search_elements', xhr: true, params: { website: 'gsm_arena', search_string: 'iphone 3g' }

        @data = JSON.parse(response.body)

        assert_equal 'card', @data['found_content']['render_content']
        assert_equal 2, @data['found_content']['content'].length
        assert_equal 'image', @data['found_content']['content'][0][0]['type_of_content']
        assert_equal 'http://cdn2.gsmarena.com/vv/bigpic/apple-iphone-3gs-ofic.jpg',
                     @data['found_content']['content'][0][0]['content'][0]['src']
        assert_equal 'link', @data['found_content']['content'][0][1]['type_of_content']
        assert_equal 'http://www.gsmarena.com/apple_iphone_3gs-2826.php',
                     @data['found_content']['content'][0][1]['content'][0]['href']
      end
    end
  end
end
