module WebResources
  module ParsingSet
    module_function

    def gsm_arena
      {
        url: 'http://www.gsmarena.com',
        search: {
          search_query: '/results.php3?sQuickSearch=yes&sName=',
          search_content: {
            type_of_content: 'card', xpath: 'div.makers ul li', set_of_data: [
              { type_of_data: 'image', xpath: 'img' },
              { type_of_data: 'link', xpath: 'a' }
            ]
          }
        },
        content: [
          { type_of_content: 'menu', xpath: 'div.brandmenu-v2 li a' },
          { type_of_content: 'menu', xpath: 'div.makers li a', paginated: 'div.nav-pages a' },
          { type_of_content: 'page', set_of_data: [
            { type_of_data: 'text', xpath: 'h1.specs-phone-name-title' },
            { type_of_data: 'image', xpath: 'div.specs-photo-main img' },
            { type_of_data: 'table', xpath: 'div#specs-list table' }
          ] }
        ]
      }
    end
  end
end
