@App ||=
  ContentBuilders: {}


$.turbo = (fn) ->
  $(document).on 'turbolinks:load', ->
    fn()
