class App.DataFetcher extends App.DataBase
  constructor: ->
    super(@website, @label)
    @_initMainMenu()

  _initMainMenu: ->
    @_fetchContentFor('0')

  _fetchContentFor: (level, customUrl = null) ->
    $.ajax
      type: 'GET'
      url: '/fetch_elements'
      data:
        website: @website
        level: level
        custom_url: customUrl if !!customUrl
      success: (data) =>
        @_dataExtractor(data)
      error: (jqXHR, textStatus) ->
        console.log textStatus

  _dataExtractor: (data) ->
    fetched_content = data.fetched_content
    typeOfContent = fetched_content.render_content
    content = fetched_content.content
    switch typeOfContent
      when 'menu'
        @_renderSelect(content)
      when 'page'
        new App.ContentBuilders.PageBuilder(content)

  _renderSelect: (elements, label = @label) ->
    builder = new App.ContentBuilders.MenuBuilder(elements, label)
    $select = $(builder.selectObject())
    @_selectHandler($select)

  _selectHandler: ($select) ->
    $select.on 'change', (e) =>
      select = e.target
      url = select.value
      @label = select.options[select.selectedIndex].text
      currentLevel = parseInt(select.dataset.level)
      @_resetNestedMenu(currentLevel)
      if !!url
        @_fetchContentFor(currentLevel + 1, url)

  _resetNestedMenu: (currentLevel) ->
    $('[data-level]').each (_, menuSelect) ->
      $menuSelect = $(menuSelect)
      if parseInt($menuSelect.attr('data-level')) > currentLevel
        $menuSelect.closest('.content-block').remove()

$.turbo ->
  new App.DataFetcher()
