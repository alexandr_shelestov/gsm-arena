class App.DataSearcher extends App.DataBase
  constructor: ->
    super(@website)
    @$searchForm = $('form#search-form')
    @$searchInput = @$searchForm.find('input')
    @$searchResultsContainer = $('#search-results')
    @_seachInputFormHandler()

  _seachInputFormHandler: ->
    @$searchForm.on 'submit', (e) =>
      e.preventDefault()
      @_searchContentFor()

  _searchContentFor: () ->
    $.ajax
      type: 'GET'
      url: '/search_elements'
      data:
        website: @website
        search_string: @$searchInput.val()
      success: (data) =>
        @$searchResultsContainer.html('')
        @_dataExtractor(data)
      error: (jqXHR, textStatus) ->
        console.log textStatus

  _dataExtractor: (data) ->
    found_content = data.found_content
    typeOfContent = found_content.render_content
    content = found_content.content
    switch typeOfContent
      when 'card'
        new App.ContentBuilders.CardBuilder(content, @$searchResultsContainer)

  _renderCard: (content) ->
    $.each content, (_index, card) =>
      console.log card

$.turbo ->
  new App.DataSearcher()
