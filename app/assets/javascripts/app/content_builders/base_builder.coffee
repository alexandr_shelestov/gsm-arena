class App.ContentBuilders.BaseBuilder

  _setLevel: ->
    $('[data-level]').length

  _toUnderscore: (string) ->
    string.toLowerCase().replace(/\ /g, '_')
