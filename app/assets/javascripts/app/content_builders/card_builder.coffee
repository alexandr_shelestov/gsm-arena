class App.ContentBuilders.CardBuilder extends App.ContentBuilders.BaseBuilder
  constructor: (cards, $searchResultsContainer) ->
    $.each cards, (_, card) =>
      image = ''
      link = ''
      $.each card, (_, element) =>
        typeOfContent = element.type_of_content
        switch typeOfContent
          when 'image'
            image = element.content[0]
          when 'link'
            link = element.content[0]

      $card = @_cardTemplate(image, link)
      $searchResultsContainer.append($card)

  _cardTemplate: (image, link) ->
    """
    <div class='col-xs-6'>
      <div class="card">
        <img class="card-img-top" src="#{image.src}" height='100px' width='auto'>
        <div class="card-block">
          <p class="card-title"><a href="#{link.href}" target='_blank'>#{link.caption}</a></p>
        </div>
      </div>
    </div>
    """
