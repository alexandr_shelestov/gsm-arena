class App.ContentBuilders.MenuBuilder extends App.ContentBuilders.BaseBuilder
  constructor: (elements, label) ->
    @$select = $(@_selectTemplate(@_optionsFrom(elements), label))
    $('fieldset').append(@$select)

  selectObject: ->
    @$select

  _selectTemplate: (options, label) ->
    name = @_toUnderscore(label)
    level = @_setLevel()
    """
      <div class='form-group row content-block'>
        <label class='col-sm-4 col-form-label' for='#{name}'>Select for #{label}</label>
        <div class='col-sm-8'>
          <select name='#{name}' id='#{name}' data-level=#{level} class='form-control'>
            #{options}
          </select>
        </div>
      </div>
    """

  _optionsFrom: (elements) ->
    options = $.map elements, (element) ->
      "<option value=#{element.href}>#{element.caption}</option>"
    options.unshift('<option value></option>')
    options.join('')
