class App.ContentBuilders.PageBuilder extends App.ContentBuilders.BaseBuilder
  constructor: (content) ->
    $pageContainer = $('<div />').attr('data-level', @_setLevel())
    $('fieldset').append($('<div />', class: 'content-block').append($pageContainer))
    $.each content, (_, element) =>
      typeOfContent = element?.type_of_content
      switch typeOfContent
        when 'table'
          @_renderTables(element.content, $pageContainer)
        when 'image'
          @_renderImages(element.content, $pageContainer)
        when 'text'
          @_renderText(element.content, $pageContainer)

  _renderText: (text, $pageContainer) ->
    $.each text, (_, text_element) ->
      $text = $("<#{text_element.tag} />", text: text_element.text)
      $pageContainer.append($text)

  _renderImages: (images, $pageContainer) ->
    $.each images, (_, image) ->
      $image = $('<img />', alt: image.alt, src: image.src)
      $pageContainer.append($image)

  _renderTables: (tables, $pageContainer) ->
    tables.map (table) =>
      $pageContainer.append(@_buildTable(table))

  _buildTable: (table) ->
    $('<table />', class: 'table table-sm').append(@_buildTr(table))

  _buildTr: (table) ->
    table.map (tr) =>
      $('<tr />').append(@_buildTh(tr).concat(@_buildTd(tr)))

  _buildTh: (tr) ->
    if tr.headers?
      tr.headers.map (header) ->
        $('<th />', scope: 'row', rowspan: header[0], text: header[1])
    else
      []

  _buildTd: (tr) ->
    if tr.rows?
      tr.rows.map (row) ->
        $('<td />', rowspan: row[0], text: row[1])
    else
      []
