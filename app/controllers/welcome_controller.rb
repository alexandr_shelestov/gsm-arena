class WelcomeController < ApplicationController

  def index
  end

  def fetch_elements
    website = params[:website]
    level = params[:level]
    custom_url = params[:custom_url]
    scraper = Scraper.new(website, level, custom_url)
    fetched_content = scraper.fetched_content

    if fetched_content.any?
      render json: { fetched_content: fetched_content }
    else
      head :bad_request
    end
  end

  def search_elements
    website = params[:website]
    search_string = params[:search_string]
    searcher = Searcher.new(website, search_string)
    fetched_content = searcher.found_content

    if fetched_content.any?
      render json: { found_content: fetched_content }
    else
      head :bad_request
    end
  end
end
