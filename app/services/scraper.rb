class Scraper
  def initialize(website, level, custom_url = nil)
    @website = WebResources::ParsingSet.send(website)
    @level = level.to_i
    @url = custom_url || @website[:url]
    @content_level = @website[:content][@level]
    @type_of_content = @content_level[:type_of_content]
  end

  def fetched_content
    {
      render_content: @type_of_content,
      content: content
    }
  end

  private

  def content
    case @type_of_content
    when 'menu'
      Extractors::Menu.new(document, @content_level[:xpath], @url).sorted_hashed_elements
    when 'page'
      Extractors::Page.new(document, @content_level[:set_of_data], @url).array_of_parsed_data
    end
  end

  def document
    @document ||= Nokogiri::HTML(response)
  end

  def with_additional_pages
    xpath_for_pages = @content_level[:paginated]
    main_page_request << Extractors::Menu.new(main_page_response, xpath_for_pages, @url)
      .sorted_hashed_elements.map do |page_link|
      request(page_link[:href])
    end.join
  end

  def response
    paginated? ? with_additional_pages : main_page_request
  end

  def main_page_response
    @main_page_response ||= Nokogiri::HTML(main_page_request)
  end

  def main_page_request
    @main_page_request ||= request
  end

  def request(url = @url)
    Curl.get(url).body_str
  end

  def paginated?
    @content_level[:paginated].present?
  end
end
