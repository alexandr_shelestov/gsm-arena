class Searcher
  def initialize(website, search_string)
    @website = WebResources::ParsingSet.send(website)
    @url = @website[:url]
    @query = search_string.split(' ').join('+')
    @search_url = @url + @website[:search][:search_query] + @query
    @search_content = @website[:search][:search_content]
    @type_of_content = @search_content[:type_of_content]
  end

  def found_content
    {
      render_content: @type_of_content,
      content: content
    }
  end

  private

  def content
    case @type_of_content
    when 'card'
      Extractors::Card.new(document, @search_content, @url).array_of_parsed_data
    end
  end

  def document
    @document ||= Nokogiri::HTML(request(@search_url))
  end

  def request(url = @url)
    Curl.get(url).body_str
  end
end
