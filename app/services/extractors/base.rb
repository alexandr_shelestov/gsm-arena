module Extractors
  class Base
    def initialize(page, xpath, url = nil)
      @page = page
      @xpath = xpath
      @url = url
    end

    private

    def absolute_url(relative_url)
      [].tap do |url|
        url << base_url
        url << relative_url
      end.join
    end

    def base_url
      @base_url ||= URI.join(@url, '/').to_s
    end

    def raw_elements(xpath = @xpath)
      @page.css(xpath)
    end
  end
end
