module Extractors
  class Page < Base
    def initialize(page, set_of_data, url)
      super(page, url)
      @set_of_data = set_of_data
    end

    def array_of_parsed_data
      @set_of_data.map do |data|
        content_builder(data)
      end
    end

    private

    def content_builder(data)
      content_type = data[:type_of_data]
      xpath = data[:xpath]
      {
        type_of_content: content_type,
        content: raw_elements(xpath).map do |element|
          element_builder(content_type, element)
        end
      }
    end

    def element_builder(content_type, element)
      case content_type
      when 'table'
        element.search('tr').map do |tr|
          {
            headers: tr.search('th').map { |th| [th[:rowspan], th.text] },
            rows: tr.search('td').map { |td| [td[:rowspan], td.text] }
          }
        end
      when 'image'
        {
          src: element[:src],
          alt: element[:alt]
        }
      when 'text'
        {
          tag: element.name,
          text: element.text
        }
      end
    end
  end
end
