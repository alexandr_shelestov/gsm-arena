module Extractors
  class Menu < Base
    def sorted_hashed_elements
      hashed_elements.sort_by{ |element| element[:caption].downcase }
    end

    private

    def hashed_elements
      raw_elements.map do |element|
        {
          caption: element.text,
          href: absolute_url(element[:href])
        }
      end
    end
  end
end
