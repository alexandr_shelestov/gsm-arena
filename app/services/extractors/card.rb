module Extractors
  class Card < Base
    def initialize(page, search_content, url)
      @page = page
      @url = url
      @search_content = search_content
      @set_of_data = @search_content[:set_of_data]
    end

    def array_of_parsed_data
      card_xpath = @search_content[:xpath]
      raw_elements(card_xpath).map do |card|
        @set_of_data.map do |data|
          content_builder(data, card)
        end
      end
    end

    private

    def content_builder(data, card)
      content_type = data[:type_of_data]
      xpath = data[:xpath]
      {
        type_of_content: content_type,
        content: raw_elements(xpath, card).map do |element|
          element_builder(content_type, element)
        end
      }
    end

    def raw_elements(xpath = @xpath, scope = @page)
      scope.css(xpath)
    end

    def element_builder(content_type, element)
      case content_type
      when 'image'
        {
          src: element[:src],
          alt: element[:alt]
        }
      when 'link'
        {
          caption: element.text,
          href: absolute_url(element[:href])
        }
      end
    end
  end
end
